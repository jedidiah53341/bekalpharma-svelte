import { writable } from 'svelte/store';

export const listOfProducts = writable([
		{id: '1',name:'Product 1',url:'/products/product1',description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."},
		{id: '2',name:'Product 2',url:'/products/product2',description:"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old."},
		{id: '3',name:'Product 3',url:'/products/product3',description:"Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de Finibus Bonorum et Malorum' (The Extremes of Good and Evil) by Cicero, written in 45 BC."},
		{id: '4',name:'Product 4',url:'/products/product4',description:"There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."},
		{id: '5',name:'Product 5',url:'/products/product5',description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, a type specimen book."},
		{id: '6',name:'Product 6',url:'/products/product6',description:"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old."},
		{id: '7',name:'Product 7',url:'/products/product7',description:"Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de Finibus Bonorum et Malorum' (The Extremes of Good and Evil) by Cicero, written in 45 BC."},
		{id: '8',name:'Product 8',url:'/products/product8',description:"There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration by injected humour, or randomised words which don't look even slightly believable."},
	]);