# bekalpharma-svelte
A small landing page adapted from a pharmaceutical company made with Svelte. I would say Svelte is even easier than Vue, in the way that Svelte's store is even simpler. Though the features remain the same with other frameworks -- like bindings, slots, computed properties, etc. Was made to get myself familiar with Svelte.
